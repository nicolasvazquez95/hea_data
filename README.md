# Trabajo Final - Introducción a las Simulaciones Numéricas

Acá guardamos datos de los logs de Lammps y de las dislocaciones levantados en Ovito.

Podemos descargarlo con un `git clone`, o con el link que corresponda.

Toda la data está es formato planilla, en formato `csv`.
